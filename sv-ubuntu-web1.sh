#!/usr/bin/env bash

# Ajout d'un nouvel utilisateur et définition du mot de passe
sudo adduser --disabled-password --gecos "" cyber_admin
echo 'cyber_admin:iopiop' | sudo chpasswd

# Assurez-vous que l'utilisateur dispose des privilèges sudo
sudo usermod -aG sudo cyber_admin

# Mise à jour et installation des paquets nécessaires
apt-get update
apt-get install -y apache2 mysql-server php libapache2-mod-php php-mysql fail2ban

# Téléchargement de WordPress
wget -P /tmp https://wordpress.org/latest.tar.gz
tar -xzvf /tmp/latest.tar.gz -C /var/www/html/

# Configuration de WordPress
sudo chown -R www-data:www-data /var/www/html/wordpress
sudo chmod -R 755 /var/www/html/wordpress

# Redémarrage du service Apache
systemctl restart apache2

# Définir la disposition du clavier en français
sudo localectl set-keymap fr

# Configuration Netplan
cat <<EOT >> /etc/netplan/01-network-manager-all.yaml
network:
  version: 2
  renderer: networkd
  ethernets:
    enp0s8:
      routes:
        - to: 0.0.0.0/0
          via: 192.168.56.253
EOT

# Configuration du DNS
cat <<EOL | sudo tee /etc/netplan/01-netcfg.yaml > /dev/null
network:
  version: 2
  ethernets:
    enp0s8:
      dhcp4: no
      addresses: [192.168.56.20/24]
      gateway4: 192.168.56.253
      nameservers:
          addresses: [1.1.1.1]
EOL

# Appliquer les modifications Netplan
sudo netplan apply

# Acces only via proxy & haproxy
sudo ufw default deny incoming
sudo ufw default allow outgoing
sudo ufw allow from 192.168.57.22 to any port 80 # IP proxy
sudo ufw allow from 192.168.57.23 to any port 80 # IP haproxy
sudo ufw enable

# Ajouter une configuration personnalisée pour fail2ban
cat <<EOT >> /etc/fail2ban/jail.d/custom.conf
[apache]
enabled = true
port = http,https
filter = apache-auth
logpath = /var/log/apache2/*access.log
maxretry = 8
findtime = 120
bantime = 40
EOT

# Redémarrer fail2ban pour appliquer les modifications
sudo service fail2ban restart

# Éteindre la machine virtuelle
sudo poweroff
