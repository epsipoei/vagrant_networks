#!/usr/bin/env bash

# Ajout d'un nouvel utilisateur et définition du mot de passe
sudo adduser --disabled-password --gecos "" cyber_admin
echo 'cyber_admin:iopiop' | sudo chpasswd

# Assurez-vous que l'utilisateur dispose des privilèges sudo
sudo usermod -aG sudo cyber_admin

# Mise à jour et installation des paquets nécessaires
apt-get update
apt-get install -y iptables keyboard-configuration

# Définir la disposition du clavier en français
sudo localectl set-keymap fr

# Récupérer l'interface de sortie numéro 1
interface_sortie=$(ip -o -4 route show to default | awk '{print $5}')

# Activation du forwarding pour permettre le routage
echo 'net.ipv4.ip_forward=1' > /etc/sysctl.d/60-ip-forward.conf
sysctl -p /etc/sysctl.d/60-ip-forward.conf

# Configuration des règles NAT
sudo iptables -t nat -A POSTROUTING -o "$interface_sortie" -j MASQUERADE

# Enregistrer les règles iptables
sudo iptables-save > /etc/iptables-rules

# Ajout du script pour restaurer les règles au démarrage
echo -e '#!/bin/bash\n/sbin/iptables-restore < /etc/iptables-rules' > /etc/network/if-pre-up.d/iptables

# Rendre le script exécutable
chmod +x /etc/network/if-pre-up.d/iptables

# Redémarrer le service réseau pour appliquer les changements
sudo netplan apply

# Ajout d'une entrée au crontab de root pour exécuter le script au redémarrage
sudo bash -c '(crontab -l ; echo "@reboot /etc/network/if-pre-up.d/iptables") | crontab -'

echo "La configuration du routeur Linux avec Ubuntu et l'activation de NAT ont été terminées avec succès."
reboot

