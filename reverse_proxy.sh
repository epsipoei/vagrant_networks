#!/bin/bash

# Ajout d'un nouvel utilisateur et définition du mot de passe
sudo adduser --disabled-password --gecos "" cyber_admin
echo 'cyber_admin:iopiop' | sudo chpasswd

# Assurez-vous que l'utilisateur dispose des privilèges sudo
sudo usermod -aG sudo cyber_admin

# Mise à jour des dépôts
sudo apt-get update

# Installation de HAProxy
sudo apt-get -y install haproxy

# Configuration du fichier haproxy.cfg
cat <<EOT | sudo tee /etc/haproxy/haproxy.cfg
global
    maxconn 4096
    daemon
    log 127.0.0.1 local0
    log 127.0.0.1 local1 notice

defaults
    log global
    mode http
    option httplog
    option dontlognull
    option forwardfor
    timeout connect 5000
    timeout client 50000
    timeout server 50000

# Ajout de la protection contre les attaques DDoS
listen stats
    bind *:1936
    mode http
    stats enable
    stats hide-version
    stats uri /haproxy_stats
    stats realm Haproxy\ Statistics
    stats auth admin:password

# Limiter le nombre de connexions par IP
    stick-table type ip size 200k expire 30m
    tcp-request connection track-sc1 src
    tcp-request connection reject if { sc1_conn_rate(60) gt 20 }

frontend http_front
    bind *:80
    default_backend http_back

backend http_back
    balance roundrobin
    server web1 192.168.56.20:80 check
    server web2 192.168.56.21:80 check
EOT

# Redémarrage du service HAProxy
sudo service haproxy restart

# Configuration Netplan
cat <<EOT >> /etc/netplan/01-network-manager-all.yaml
network:
  version: 2
  renderer: networkd
  ethernets:
    enp0s8:
      routes:
        - to: 0.0.0.0/0
          via: 192.168.57.253
EOT

# Configuration du DNS
cat <<EOL | sudo tee /etc/netplan/01-netcfg.yaml > /dev/null
network:
  version: 2
  ethernets:
    enp0s8:
      dhcp4: no
      addresses: [192.168.57.23/24]
      gateway4: 192.168.57.253
      nameservers:
          addresses: [1.1.1.1]
EOL

# Appliquer les modifications Netplan
sudo netplan apply

# Éteindre la machine virtuelle
sudo poweroff
